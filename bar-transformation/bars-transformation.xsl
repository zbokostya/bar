<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/">
        <html>
            <body>
                <table border="1">
                    <h3>Все бары Минска</h3>
                    <tr>
                        <td>Название</td>
                        <td>Рейтинг утром</td>
                        <td>Рейтинг днем</td>
                        <td>Рейтинг вечером</td>
                        <td>Рейтинг ночью</td>
                        <td>Id</td>
                    </tr>
                    <xsl:apply-templates select="//bar" mode="one"/>
                </table>
            <h3>Сортировка по рейтингу вечером</h3>
                <table border="1">
                    <tr>
                        <td>Название</td>
                        <td>Рейтинг вечером</td>
                    </tr>
                    <xsl:apply-templates select="//bar" mode="second">
                        <xsl:sort select="@eveningRate" data-type="number" order="descending"/>
                    </xsl:apply-templates>
                </table>
                <h3>Количество баров с ночным рейтингом >5 : <xsl:value-of select="count(//bar[@nightRate &gt; 5])"/></h3>

            </body>
        </html>
    </xsl:template>

    <xsl:template match="bar" mode="one">
        <tr>
            <td><a href="//{@name}.com/{generate-id(.)}"><xsl:value-of select="@name"/></a></td>
            <td><xsl:value-of select="@morningRate"/></td>
            <td><xsl:value-of select="@afternoonRate"/></td>
            <td><xsl:value-of select="@eveningRate"/></td>
            <td><xsl:value-of select="@nightRate"/></td>
            <td><xsl:value-of select="generate-id(.)"/></td>
        </tr>
    </xsl:template>

    <xsl:template match="bar" mode="second">
        <tr>
            <td>
                <xsl:value-of select="@name"/>
            </td>
            <td>
                <xsl:value-of select="@eveningRate"/>
            </td>
        </tr>
    </xsl:template>


</xsl:stylesheet>